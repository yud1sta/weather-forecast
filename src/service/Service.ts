import axios from 'axios';

export default class Service {
  baseURL = 'https://api.openweathermap.org/data/2.5/'
  async getAll(url: string, config = {}) {
    try {
      const result = await axios.get(`${this.baseURL}${url}`, config );
      return result.data
    } catch (error) {
     console.log(error)
    }
  }
}
