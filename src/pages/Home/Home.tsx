import WeatherItem from '../../components/molecules/weatherItem/WeatherItem';
import NextForecast from '../../components/molecules/nextForecast/NextForecast';
import WeatherMain from '../../components/molecules/weatherMain/WeatherMain';
import WeatherDetail from '../../components/molecules/weatherDetail/WeatherDetail';
import { useSelector } from 'react-redux';
import { RootState } from '../../store/store';
import { IDataWeather } from '../../store/weather/weatherSlice';
import { formatHourMinute } from '../../utils/date';

const Home = () => {
  const forecastHourly = useSelector(
    (state: RootState) => state.weather.forecastHourly
  );
  return (
    <div className="container">
      <WeatherMain />
      <WeatherDetail />
      <div className="row mt-20 gap-20">
        {forecastHourly.map((e: IDataWeather, index: number) => (
          <WeatherItem
            labelHour={formatHourMinute(new Date(e.date))}
            labelDegrees={`${e.temperature}°`}
            icon={e.icon}
            className={index > 3 ? 'sm-hide': ''}
            key={e.date}
          />
        ))}
      </div>
      <NextForecast />
    </div>
  );
};

export default Home;
