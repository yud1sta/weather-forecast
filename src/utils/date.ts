const padTo2Digits = (num: number) => {
  return String(num).padStart(2, '0');
};
export const isSameDate = (date1: Date, date2: Date) => {
  return (
    date1.getFullYear() === date2.getFullYear() &&
    date1.getMonth() === date2.getMonth() &&
    date1.getDate() === date2.getDate()
  );
};

export const formatHourMinute = (date: Date) => {
  return `${padTo2Digits(date.getHours())}:${padTo2Digits(date.getMinutes())}`;
};

export const formatDay = (date: Date) => {
  return date.toLocaleString('en-us', { weekday: 'long' });
};
