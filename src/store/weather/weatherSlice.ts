import { createAsyncThunk, createSlice } from '@reduxjs/toolkit';
import Service from '../../service/Service';
import { formatDay, isSameDate } from '../../utils/date';

const service = new Service();

const generateDataWeather = (payload: any, isOnlyDay = false) => ({
  humidity: payload.main?.humidity,
  lowTemp: payload.main?.temp_min,
  heightTemp: payload.main?.temp_max,
  temperature: payload.main?.temp,
  windSpeed: payload.wind?.speed,
  icon: payload.weather[0]?.icon,
  mainWeather: payload.weather[0]?.description,
  pressure: payload.main?.pressure,
  date: isOnlyDay ? formatDay(new Date(payload.dt_txt)) : payload.dt_txt,
});
export const getCurrentWeather = createAsyncThunk(
  'weather/getCurrent',
  async ({ lat, lon }: any) => {
    const response = await service.getAll(
      `weather?lat=${lat}&lon=${lon}&appid=${process.env.REACT_APP_API_WEATHER_KEY}&units=metric`
    );
    return response;
  }
);

export const getForecastWeather = createAsyncThunk(
  'weather/getForecast',
  async ({ lat, lon }: any) => {
    const response = await service.getAll(
      `forecast?lat=${lat}&lon=${lon}&appid=${process.env.REACT_APP_API_WEATHER_KEY}&units=metric`
    );
    return response.list;
  }
);

export interface IDataWeather {
  humidity: number;
  lowTemp: number;
  heightTemp: number;
  temperature: number;
  windSpeed: number;
  icon: string;
  mainWeather: string;
  pressure: number;
  date: string;
}

export interface weatherState {
  currentWeather: IDataWeather;
  forecastHourly: Array<IDataWeather>;
  forecastDaily: Array<IDataWeather>;
}

const initDataWeather = {
  humidity: 0,
  lowTemp: 0,
  heightTemp: 0,
  temperature: 0,
  windSpeed: 0,
  icon: '01d',
  mainWeather: '',
  pressure: 0,
  date: '',
};

const initialState: weatherState = {
  currentWeather: initDataWeather,
  forecastDaily: [initDataWeather],
  forecastHourly: [initDataWeather],
};

export const counterSlice = createSlice({
  name: 'counter',
  initialState,
  reducers: {},
  extraReducers: (builder) => {
    builder.addCase(getCurrentWeather.fulfilled, (state, action) => {
      state.currentWeather = generateDataWeather(action.payload);
    });
    builder.addCase(getForecastWeather.fulfilled, (state, action) => {
      const data: Array<IDataWeather> = [];
      const dataDaily: Array<IDataWeather> = [];
      action.payload.forEach((e: any, index: number) => {
        if (isSameDate(new Date(e.dt_txt), new Date())) {
          data.push(generateDataWeather(e));
        }
        if (
          !isSameDate(new Date(e.dt_txt), new Date()) &&
          new Date(e.dt_txt).getHours() === 6
        ) {
          dataDaily.push(generateDataWeather(e, true));
        }
      });
      state.forecastHourly = data;
      state.forecastDaily = dataDaily;
    });
  },
});

export default counterSlice.reducer;
