interface IProps {
  variant?: string;
  size: number;
  children: string;
  color?: string;
  className?: string;
}

const Text = (props: IProps) => {
  const {
    variant = '',
    size,
    children,
    color = 'white',
    className = '',
  } = props;
  return <p className={`font-${size} ${variant} ${color} ${className}`}>{children}</p>;
};

export default Text;
