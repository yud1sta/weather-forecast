import clearImage from '../../../assets/weather/clear.png';
import Text from '../../atoms/text/Text';

interface IProps {
  labelHour: string;
  labelDegrees: string;
  icon: string;
  className: string;
}
const WeatherItem = (props: IProps) => {
  const { labelHour, labelDegrees, icon, className } = props;

  return (
    <div className={`weather-item bg-dark column p-20 ${className}`}>
      <Text size={27} className="flex-1 flex jc-center">
        {labelHour}
      </Text>
      <div className="flex jc-center">
        <img
          src={`http://openweathermap.org/img/wn/${icon}@4x.png`}
          className="width-68"
        />
      </div>
      <Text size={27} className="flex-1 flex jc-center ai-end">
        {labelDegrees}
      </Text>
    </div>
  );
};

export default WeatherItem;
