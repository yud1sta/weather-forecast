import { useSelector } from 'react-redux';
import { RootState } from '../../../store/store';
import Text from '../../atoms/text/Text';
import WeatherDetailItem from './WeatherDetailItem';

const WeatherDetail = () => {
  const currentWeather = useSelector(
    (state: RootState) => state.weather.currentWeather
  );
  return (
    <div className="mt-20 row jc-sb sm-hide">
      <WeatherDetailItem label={`${currentWeather.windSpeed} mps`} value="Wind"/>
      <WeatherDetailItem label={`${currentWeather.lowTemp}°`} value="Low"/>
      <WeatherDetailItem label={`${currentWeather.heightTemp}°`} value="Height"/>
      <WeatherDetailItem label={`${currentWeather.humidity}%`} value="Humidity"/>
      <WeatherDetailItem label={`${currentWeather.pressure} hPa`} value="Pressure"/>
    </div>
  );
};

export default WeatherDetail;
