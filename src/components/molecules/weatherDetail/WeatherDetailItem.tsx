import Text from '../../atoms/text/Text';

interface IProps {
  label: string;
  value: string;
}
const WeatherDetailItem = (props: IProps) => {
  const { label, value } = props;
  return (
    <div>
      <Text size={27} className="lh-90-percent">
        {label}
      </Text>
      <Text size={27} className="opacity-70-percent">
        {value}
      </Text>
    </div>
  );
};

export default WeatherDetailItem;
