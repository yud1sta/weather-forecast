import Text from '../../atoms/text/Text';
import clearImage from '../../../assets/weather/clear.png';

interface IProps {
  labelDay: string;
  labelDegree: string;
  icon: string;
}
const NextForecastlItem = (props: IProps) => {
  const { labelDay, labelDegree, icon } = props;
  return (
    <div className="row ai-center mt-20">
      <Text size={27} className="flex-1 flex">
        {labelDay}
      </Text>
      <div className="flex-1">
        <img
          src={`http://openweathermap.org/img/wn/${icon}@4x.png`}
          className="width-47"
        />
      </div>
      <Text size={27} className="flex-1 flex jc-end">
        {labelDegree}
      </Text>
    </div>
  );
};

export default NextForecastlItem;
