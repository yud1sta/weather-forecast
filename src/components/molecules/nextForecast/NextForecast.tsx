import { useSelector } from 'react-redux';
import { RootState } from '../../../store/store';
import { IDataWeather } from '../../../store/weather/weatherSlice';
import Text from '../../atoms/text/Text';
import NextForecastlItem from './NextForecastItem';

const NextForecast = () => {
  const forecastDaily = useSelector(
    (state: RootState) => state.weather.forecastDaily
  );
  return (
    <div className="next-forecast-container bg-dark ph-20 column">
      <Text size={27} variant="bold" className="mt-20">
        Next Forecast
      </Text>
      {forecastDaily.map((e: IDataWeather) => (
        <NextForecastlItem
          labelDay={e.date}
          labelDegree={`${e.lowTemp}° - ${e.heightTemp}°`}
          icon={e.icon}
          key={e.date}
        />
      ))}
    </div>
  );
};

export default NextForecast;
