import { useEffect, useState } from 'react';
import clearImage from '../../../assets/weather/clear.png';
import Text from '../../atoms/text/Text';
import { IconMap } from '../../atoms/icons/Icons';
import { useSelector, useDispatch } from 'react-redux';
import {
  getCurrentWeather,
  getForecastWeather,
} from '../../../store/weather/weatherSlice';
import { AppDispatch, RootState } from '../../../store/store';
import PlacesAutocomplete from '../PlacesAutocomplete/PlacesAutocomplete';

const WeatherMain = () => {
  const [cityName, setCityName] = useState('Jakarta');
  const [locations, setLocations] = useState([-6.2087634, 106.845599]);
  const currentWeather = useSelector(
    (state: RootState) => state.weather.currentWeather
  );
  const dispatch = useDispatch<AppDispatch>();

  useEffect(() => {
    const intervalId = setInterval(() => {
      dispatch(getCurrentWeather({ lat: locations[0], lon: locations[1] }));
      dispatch(getForecastWeather({ lat: locations[0], lon: locations[1] }));
    }, 1000 * 5 * 60);
    return () => clearInterval(intervalId);
  }, []);

  useEffect(() => {
    dispatch(getCurrentWeather({ lat: locations[0], lon: locations[1] }));
    dispatch(getForecastWeather({ lat: locations[0], lon: locations[1] }));
  }, [locations[0], locations[1]]);

  return (
    <div className="br-20">
      <div className="row ai-center gap-20 mt-20">
        <IconMap />
        <Text size={21}>{cityName}</Text>
        <PlacesAutocomplete
          setCityName={setCityName}
          setLocations={setLocations}
        />
      </div>
      <div className="row mt-20">
        <img
          src={`http://openweathermap.org/img/wn/${currentWeather.icon}@4x.png`}
        />
        <div className="column flex-1 jc-center">
          <Text size={49} className="lh-90-percent">
            {`${currentWeather.temperature}°`}
          </Text>
          <Text size={35}>{currentWeather.mainWeather}</Text>
        </div>
      </div>
    </div>
  );
};

export default WeatherMain;
